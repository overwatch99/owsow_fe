import "../../scss/header/index.css";
import logoImg from "./images/brandLogo.png";
function Header() {
  return (
    <div className="header">
      <div className="header-container">
        <div className="header-row">
          <div className="header-languages">
            <select>
              <option>Select Language</option>
              <option>English</option>
            </select>
          </div>
          <div className="header-login">
            <i class="fal fa-lock-alt"></i> <span>Đăng nhập</span>
          </div>
        </div>
        <div className="header-row">
          <div className="header-brand">
            <img src={logoImg} alt="logo" />
          </div>
          <div className="header-search">
            <input placeholder="TÌM KIẾM SẢN PHẨM" type="text" />
            <i class="far fa-search"></i>
          </div>
          <div className="header-userInfo">
            <div className="header-userInfo-cart">
              <i class="fal fa-shopping-bag"></i>
              <p>Giỏ hàng</p>
              <span>0</span>
            </div>
            <div className="header-userInfo-account">
              <i class="fal fa-user"></i>
              <p>Tài khoản</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Header;
